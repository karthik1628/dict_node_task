'use strict'
var {to, TE} = require('../utils/utilservice');
const ApiService = require('./HerokuApiService');
const fetch = require("node-fetch");
const CONFIG = require('../config/appconfig');
var waterfall = require('async-waterfall');



var self=module.exports = {
    getDefinitions:async function(word){
        var apiUrl      = CONFIG.API_URL.BASE_URL + word + CONFIG.API_URL.DEFINITIONS + CONFIG.API_KEY;
        let err, apiResponse;
        [err, apiResponse]= await to(fetch(apiUrl));
        if(err){
            TE(err.message);
        }
        let WordDefs = await apiResponse.json();
        if(WordDefs) return WordDefs;
    },

    getSynonyms:async function(word){
        var apiUrl  = CONFIG.API_URL.BASE_URL + word + CONFIG.API_URL.RELATEDWORD + CONFIG.API_KEY;
        let err, apiResponse;
        [err, apiResponse]= await to(fetch(apiUrl));
        if(err){
            TE(err.message);
        }
        let respData = await apiResponse.json();
        if(respData){
            let wordData;
            respData.forEach((element)=>{
                if(element.relationshipType=="synonym"){
                    wordData =element.words;
                }
            });
            return (wordData)? wordData:[];
        }else{
            return false;
        }
    },

    getAntonyms: async function(word){
        var apiUrl      = CONFIG.API_URL.BASE_URL + word + CONFIG.API_URL.RELATEDWORD + CONFIG.API_KEY;
        let err, apiResponse;
        [err, apiResponse]= await to(fetch(apiUrl));
        if(err) TE(err.message);
        let data = await apiResponse.json();
        if(data){
            // console.log(data[0].words);
            
            // return (data[0].words) ? data[0].words:[];antonym
            let wordData;
            data.forEach((element)=>{
                if(element.relationshipType=="antonym"){
                    wordData =element.words;
                }
            });
             return (wordData)? wordData:[];
        }else{
            return false;
        }
        
    },

    getExamples: async function(word){
        var apiUrl = CONFIG.API_URL.BASE_URL + word + CONFIG.API_URL.EXAMPLES + CONFIG.API_KEY;
        let err, apiResponse;
        [err, apiResponse]= await to(fetch(apiUrl));
        if(err) TE(err.message);
        if(apiResponse){
            let exData = await apiResponse.json();
            return exData.examples;
        }else{
            return false;
        }
    },


    wordDictionary:async function(word){
        let err, data;
        // if(word==undefined){
        //     word=await self.getRandomWord();
        // }
        [err, data] = await to(Promise.all([self.getSynonyms(word),self.getAntonyms(word),self.getDefinitions(word),self.getExamples(word)]));
        if(err) TE(err.message);
        if(data){
            return data;
        }else{
            return false;
        }
    },

    getRandomWord:async function(){
        let err, response, randomWord;
        let apiUrl="https://fourtytwowords.herokuapp.com/words"+CONFIG.API_URL.RANDOM + CONFIG.API_KEY;
        [err, response]= await to(fetch(apiUrl));
        if(response){
            let randomWord = await response.json();
            return randomWord.word;
        }
    }

};