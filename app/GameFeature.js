'use strict'
var {to, TE, showArrayData} = require('../utils/utilservice');
const ApiService = require('./HerokuApiService');
const AppMessages = require('../utils/appMessage');
let GameHelpers = require('../utils/gameHelper');


var self = module.exports = {
    intializeGame:async function(line){
        GameHelpers.GAME_STATE=true;
        let err, data, randomWord;
        [err, randomWord]= await to(ApiService.getRandomWord());
        if(err||!randomWord) console.error(AppMessages.ENTER_ANSWER.GAME_START_ERROR+'\n'+"Please enter 3 to quit");
        [err, data]= await to(ApiService.wordDictionary(randomWord));
        if(err) console.error(AppMessages.ENTER_ANSWER.GAME_START_ERROR+'\n'+"Please enter 3 to quit");
        if(data){
            GameHelpers.WORD=randomWord;
            GameHelpers.SYNONYMS=data[0];
            GameHelpers.ANTONYMS=data[1];
            GameHelpers.DEFINITIONS=data[2];
            console.log(AppMessages.QUESTION_HEADING);
            console.log("-------------------------------------------");
            if(data[0].length>0)   console.log("Synonym :"+data[0][self.getRandomInt(data[0].length)]);
            if(data[1].length>0)   console.log("Antonym :"+data[1][self.getRandomInt(data[1].length)]);
            if(data[2].length>0)   console.log("Definition :"+data[2][self.getRandomInt(data[2].length)]['text']);
            console.log(AppMessages.ENTER_ANSWER);
        }
    },
   // AppMessages
    checkAnswer:async function(line){
        if(typeof line ==="string"){
            if(line==GameHelpers.WORD||GameHelpers.SYNONYMS.includes(line)||GameHelpers.ANTONYMS.includes(line)){
                console.log("Huuray..!, That's A Correct Answer");
                self.quitGame();
            }else{
                console.warn("Wrong Answer.. :(, Try Again");
            }
           
        }else{
           console.warn(AppMessages.ENTER_ANSWER.GAME_OPTIONS);            
        }
    },

    quitGame: async function(line){
        console.log(AppMessages.GAME_QUIT +"\n Correct answer is "+GameHelpers.WORD);           
        let err, fullDict;
        [err, fullDict]= await to(ApiService.wordDictionary(GameHelpers.WORD));
        if(err) console.log(err.message);
        if(fullDict){
            showArrayData("SYNONMYS",fullDict[0]);
            showArrayData("ANTONMYS",fullDict[1]);
            showArrayData("DEFINITIONS",fullDict[2]);
            showArrayData("EXAMPLES",fullDict[3]);
        }

        GameHelpers.GAME_STATE    =false;
        GameHelpers.WORD          = null;
        GameHelpers.DEFINITIONS   = [];
        GameHelpers.SYNONYMS      = [];
        GameHelpers.ANTONYMS      = [];
        GameHelpers.HINT_COUNTER  = 0;

        // Exiting the current process;
        inputInterface.close();
    },

    /**
     *  displays message try agin to user, in response to user inout 1
     */

    nextChance: function(){
        console.log(AppMessages.TRY_AGAIN);
    },

    getHint: function(gameState){
        var hintCount   = GameHelpers.HINT_COUNTER,
            hint        = [],
            message;

        switch(hintCount){
          case 0:
              hint.push(self.jumbleWord(GameHelpers.WORD));
              message   = AppMessages.HINTS.JUMBLE_WORD;
              break;
          case 1:
              hint.push(GameHelpers.DEFINITIONS[self.getRandomInt(GameHelpers.DEFINITIONS.length)]['text']);
              message   = AppMessages.HINTS.DEFINITION;
              break;
          case 2:
              hint.push(GameHelpers.SYNONYMS[self.getRandomInt(GameHelpers.SYNONYMS.length)]);
              message   = AppMessages.HINTS.SYNONYM;
              break;
          case 3:
              hint.push(GameHelpers.ANTONYMS[self.getRandomInt(GameHelpers.SYNONYMS.length)]);
              message   = AppMessages.HINTS.ANTONYM;
              break;
        }
        console.log(message);
        console.log(hint[0]);
        if(hintCount ==3)
            GameHelpers.HINT_COUNTER = 0;
        else
            GameHelpers.HINT_COUNTER = GameHelpers.HINT_COUNTER + 1;
        
      },

    getRandomInt: function(maxNum){
        return Math.floor(Math.random() * Math.floor(maxNum));
    },

    jumbleWord :  function(word){
        var wordsArray  = word.split('');
        var shuffledWord = '';
        word = word.split('');
        while (word.length > 0) {
        shuffledWord +=  word.splice(word.length * Math.random() << 0, 1);
        }
        return shuffledWord;
    }


};
