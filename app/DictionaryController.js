'use strict'
var {to, TE} = require('../utils/utilservice');
const ApiService = require('./HerokuApiService');
  /**
   * Controller for Organiging Calls to Api Services.
   * @param {String} word
   */


module.exports = {
    wordDefinition:async function(word){
        let err, response;

        [err, response]= await to(ApiService.getDefinitions(word));
        if(err){
            TE(err.message);
        } 
        if (response) {
           return response;
        }else {
            console.warn('Oops! The word doesn\'t exist or Something went wrong!!!');
        }
    },

    wordSynonyms: async function(word){
        let err, WordSynonmys;

        [err, WordSynonmys]= await to(ApiService.getSynonyms(word));
        if(err) TE(err.message);
        if(WordSynonmys){
            var synonyms = "";
            WordSynonmys.forEach((obj) => {
                synonyms += obj + "\n";
            });
            return synonyms;
         }else {
             console.warn("Oops! Synonmys won't exist or Word doesn't exist");
         }
    },

    displayAntonyms: async function(word){
        let err, WordAntonyms;

        [err, WordAntonyms]= await to(ApiService.getAntonyms(word));
        if(err) TE(err.message);
        if(WordAntonyms&&WordAntonyms!==false) {
            var antonyms = "";
            WordAntonyms.forEach((obj) => {
                antonyms += obj + "\n";
            });
            return antonyms;
         }else {
             console.warn("Oops! Antonyms won't exist or Can't find word");
         }
    },


    displayExamples: async function(word){
        let err, data, exmp="";

        [err, data]= await to(ApiService.getExamples(word));
        if(err) TE(err.message);
        if(data&&data!==false) {
            console.log(word+" Examples:");
            data.forEach(element => {
                exmp += element.text + "\n";
            });
            return exmp;
         }else {
             console.warn("Can't fetch examples of "+word);
         }
    },


    displayFullDictionary: async function(DictWord){
        let err, dictionary;

        [err, dictionary]= await to(ApiService.wordDictionary(DictWord));
        if(err) TE(err.message);
        if(dictionary && dictionary!== false){
            return dictionary;
        }else{
            console.warn("Unable to fetch Dictionary "+word);
        }
    },





};
