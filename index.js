#!/usr/bin/env node
'use strict'
const program = require('commander');
const DictionaryController = require('./app/DictionaryController');
const {to, TE, showArrayData} = require('./utils/utilservice');
const ApiService = require('./app/HerokuApiService');
let GameHelpers = require('./utils/gameHelper');
var READLINE       = require('readline');
const GameFeature = require('./app/GameFeature');
const CONFIG = require('./config/appconfig');

// if (process.argv.length == 2) {
//     console.log('Getting word of the day...');
//     wordnikApi.wordOfTheDay();
// }
program
  .version('0.1.0')
  .arguments('[command] [word]')
  .action(async function (command, word) {
  //console.log(command, word);
  if(command==undefined){
    GameHelpers.GAME_ENABLED=true;
  }
    switch (command) {
        case "def":
            {
                let err, wordDef;
                [err, wordDef]= await to(DictionaryController.wordDefinition(word));
                if(err) console.log(err.message);
                if(wordDef) console.log(wordDef);
                break;
            }
        case "syn":
            {
              let err, wordDef;
              [err, wordDef]= await to(DictionaryController.wordSynonyms(word));
              if(err) console.log(err.message);
              if(wordDef) console.log(wordDef);
              break;
            }
            
        case "ant":
            {
              let err, wordDef;
              [err, wordDef]= await to(DictionaryController.displayAntonyms(word));
              if(err) console.log(err.message);
              if(wordDef) console.log(wordDef);
              break;
            }
        case "ex":
            {
              let err, wordDef;
              [err, wordDef]= await to(DictionaryController.displayExamples(word));
              if(err) console.log(err.message);
              if(wordDef) console.log(wordDef);
              break;
            }
        // case "dict":
        //     {
        //       let err, wordDef;
        //       [err, wordDef]= await to(DictionaryController.displayFullDictionary(word));
        //       if(err) console.log(err.message);
        //       if(wordDef) console.log(wordDef);
        //       break;
        //     }
        case "play":
                // creating a command line interface for interaction with user
                global.inputInterface = READLINE.createInterface({
                  input: process.stdin,
                  output: process.stdout,
                  prompt: 'dict> '
                });
                inputInterface.on('line',async (line) => {
                   switch (line) {
                       case CONFIG.GAME_COMMANDS.TRY_AGAIN:
                            GameFeature.nextChance(line);
                           break;
                        case CONFIG.GAME_COMMANDS.HINT:
                            GameFeature.getHint();
                            break;
                        case CONFIG.GAME_COMMANDS.QUIT:
                            GameFeature.quitGame();
                                break;
                       default:
                            GameFeature.checkAnswer(line);
                   }
                  inputInterface.prompt();
                });

              await GameFeature.intializeGame();
                // listener when input stream receives a <ctrl>-C input
                inputInterface.on('SIGINT', () => {
                  inputInterface.question('Are you sure you want to exit app? ', (answer) => {
                    if (answer.match(/^y(es)?$/i)) inputInterface.pause();
                  });
                });
           
             break;
            
            default:
            {
                if(!GameHelpers.GAME_STATE){
                    if (word==undefined) word = command;
                    if(word==undefined && command==undefined){
                        word=await ApiService.getRandomWord();
                    }
                    let err, wordDef;
                    [err, wordDef]= await to(DictionaryController.displayFullDictionary(word));
                    if(err) console.log(err.message);
                    if(wordDef){
                    showArrayData("SYNONMYS",wordDef[0]);
                    showArrayData("ANTONMYS",wordDef[1]);
                    showArrayData("DEFINITIONS",wordDef[2]);
                    showArrayData("EXAMPLES",wordDef[3]);
                    }
                }
                break;
            }
    }
}).parse(process.argv);
