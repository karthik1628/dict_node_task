const {to} = require('await-to-js');
var _ = require('underscore');
const pe = require('parse-error');


module.exports.to = async (promise) => {
    let err, res;
    [err, res] = await to(promise);
    if(err) return [pe(err)];

    return [null, res];
};


module.exports.TE = TE = function(err, log){ // TE stands for Throw Error
    // var error = new Error();
    if(log === true){
        console.error(err);
    }
    if (err instanceof Error) {
        throw new Error(err.message);
    } else if (err) {
        throw new Error(err);
    }
};

module.exports.showArrayData= showArrayData= function(heading, array){
    if(array.length == 0)
      return;
    // printing heading
    console.log('------------------------------------------------------------------');
    console.log(heading + '\n');
    // printing data
    array.forEach((el) => {
      if(heading=="SYNONMYS"||heading=="ANTONMYS"){
        console.log(el + '\n');
      }else{
        console.log(el.text + '\n');
      }
    });
};