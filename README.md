# Automate.io - Backend Dev - Task
# dict
Command Line Dictionary Tool

## Setup
1. Install node, npm.
2. Download the repository, unzip the repository.
4. Go to root folder and run below command.
    ```
    npm link
    ```
3. The possible commands are,
    * *dict def ```<word>```*
    * *dict syn ```<word>```*
    * *dict ant ```<word>```*
    * *dict ex ```<word>```*
    * *dict ```<word>```*
    * *dict play*
    * *dict*